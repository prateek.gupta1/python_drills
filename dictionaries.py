#Write a function to find the number of occurrences of each word in a string
#(Don't consider punctuation characters)

def occur(str1):
    d = {}
    str2 = str1.replace("," , "")
    words = str2.split()

    for elem in words:
        if elem in d:
            d[elem] += 1
        else:
            d[elem] = 1

    print(d)

#occur(str1)
occur("Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported.")

"""
{'Python': 3, 'is': 2, 'an': 1, 'interpreted,': 1, 'high-level,': 1,
'general-purpose': 1, 'programming': 4, 'language.': 2, 'interpreters': 1,
'are': 2, 'available': 1, 'for': 1, 'many': 1, 'operating': 1, 'systems.': 1,
'a': 1, 'multi-paradigm': 1, 'Object-oriented': 1, 'and': 1, 'structured': 1,
'fully': 1, 'supported.': 1}

"""

#Define a function that prints all the items (keys, values) in a dictionary

def print_dict_keys_and_values(d):
    print(d.items())


print_dict_keys_and_values({'a':1 , 'b':2, 'c':3})


#Define a function that returns a list of all the items in a dictionary sorted by the dictionary's keys.

def sorted_dict_keys(d):
    #list =[]  
    for key in sorted(d):
        #print(d)
        #list.append(key)
        print( "(%s: %s)" % (key, d[key]),end=" ")
     

sorted_dict_keys({'c':2,'b':3,'a':1})




    
