"""
Create a class called Point which has two instance variables, x and y that represent the x and y co-ordinates respectively.
Initialize these instance variables in the __init__ method
"""

class Point:
    def __init__(self,x,y):
        self.x = x
        self.y = y

coord = Point(3.0,4.0)
print(coord.x,coord.y)


"""
Define a method, distance on Point which accepts another Point
object as an argument and returns the distance between the two points.
"""
import math
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def dist(self):
        print(self.x,self.y)
        
    def distance(self,new_point):
        res = math.sqrt((new_point.x - self.x)**2 + (new_point.y - self.y)**2)
        print(res)


point1 = Point(5,8)
point2 = Point(8,12)

point1.dist()
point1.distance(point2)
