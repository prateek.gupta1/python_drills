#Save the first n natural numbers
#and their squares into a file in the csv format.

#def numbers_and_squares(n, file_path):
import csv   
def square(n):
    return n*n

def squares(n):
    rows = []
    for i in range(1,n+1):
        rows.append(i)
        rows.append(square(i))
    return rows

with open('Answer.csv', 'a') as file:
    writer1 = csv.writer(file)
    writer1.writerow(squares(5))
    file.close()
