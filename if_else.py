x = 1
y = 10
if x > y:
    x = y
else:
    y = x + 1

print(y)

#What is y after the above code is executed?
 #  Ans--->  y = 2
