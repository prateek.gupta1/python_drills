"""
Define a function that does the equivalent of uniq <file_path> | sort, i.e., the function should

Accept a file path as the argument
Read the contents of the file into a string
Split the string into lines
Remove duplicate lines
Sort the lines
Return the sorted lines
"""


import os.path
def operation(path):
    file = path + "a.txt"
    f = open(file)
    #print(f.read())

    str = f.read()
    print(str)

    str1 = str.splitlines()
    print(str1)

    print(list(set(str1)))

    str2 = f.readlines()
    str2.sort()
    for i in range(len(str2)):
        print(str2[i])


path = "C:\\Users\\hp1\\AppData\\Local\\Packages\\CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc\\LocalState\\rootfs\\home\\prateek\\python_drills\\"
#path = "C:\\Desktop\\"
operation(path)


