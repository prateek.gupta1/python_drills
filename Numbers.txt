 No, you can not cause integer overflow in Python.


>>> 7/2
3.5
>>> 7//2
3
>>> -7/2
-3.5
>>> -7//2
-4
>>> 
