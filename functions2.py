"""
To the n_digit_primes function, set the default value of argument n to 2


Call the n_digit_primes with a keyword argument, n_digit_primes(n=1)


Define a function, args_sum that accepts an arbitrary number of integers as input and computes their sum.


Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute. If absolute is True, then args_sum must return the absolute value of the sum of *args. If absolute is not specified,
then return the sum without performing any conversion.

"""
#To the n_digit_primes function, set the default value of argument n to 2

def n_digit_primes(n):
    primes = []
    # your logic here
    for num in range(10**(n-1),10**n):
        if num>1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print(num)
    return primes

n_digit_primes(2)

print()
#Call the n_digit_primes with a keyword argument, n_digit_primes(n=1)


def n_digit_primes(n):
    primes = []
    # your logic here
    for num in range(10**(n-1),10**n):
        if num>1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print(num)
    return primes

n_digit_primes(n=1)

print()

#Define a function, args_sum that accepts an arbitrary
#number of integers as input and computes their sum.


def args_sum(*args):
    sum = 0
    for i in args:
        sum = sum+i
    print(sum)

args_sum(3,5,7,8)
args_sum(1,2,6,7,8,9,4)

print()

"""
Modify the args_sum function so that it accepts an optional, boolean, keyword argument named absolute.
If absolute is True, then args_sum must return the absolute value of the sum of *args.
If absolute is not specified, then return the sum without performing any conversion.
"""

def args_sum(*args, **absolute):
    sum = 0
    if absolute == True:
        for i in args:
            sum = sum + abs(i)
        print(sum)
    else:
        for i in args:
            sum = sum + i
        print(sum)

args_sum(3,-5,9,-2, True)
        
    
