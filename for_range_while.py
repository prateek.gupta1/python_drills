x = [1, 2, 3, 4]


#Print all the elements in x

print(x)  # [1, 2, 3, 4]

#Print all the elements and their indexes using the enumerate function

x1 = enumerate(x)
print(list(x1))    #[(0, 1), (1, 2), (2, 3), (3, 4)]


#Print all the integers from 10 to 0 in descending order using range

for i in range(10,0,-1):
    print(i, end=" ")    # 10 9 8 7 6 5 4 3 2 1


print()

# Print all the integers from 10 to 0 in descending order using while

num = 10
while num>=1:
    print(num, end = ' ')
    num = num-1                    # 10 9 8 7 6 5 4 3 2 1
    
print() 

