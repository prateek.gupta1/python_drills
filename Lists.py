x = [3,1,2,4,1,2]

# Find the sum of all the elements in x
def addition(x):
    add = 0
    for i in x:
        add = add + i
    print(add)
addition(x)   # 13


# Find the length of x

print(len(x))  # 6


# Print the last three items of x

print(x[-3:])  # [4, 1, 2]

# Print the first three items of x

print(x[:3])  # [3, 1, 2]


# Sort x

x.sort()
print(x)   # [1, 1, 2, 2, 3, 4]


# Add another item, 10 to x

x.append(10)
print(x)     # [1, 1, 2, 2, 3, 4, 10]


# Add another item 11 to x

x.append(11)
print(x)     # [1, 1, 2, 2, 3, 4, 10, 11]

# Remove the last item from x

x.pop()
print(x)    # [1, 1, 2, 2, 3, 4, 10]


# How many times does 1 occur in x?

print(x.count(1))   # 2


# Check whether the element 10 is present in x

def check(elem):
    if elem in x:
        print("Exist")
    else:
        print("Not Exist")

check(10)   # Exist






y = [5,1,2,3]

#Add all the elements of y to x

#for elem in y:
#    x.append(elem)
x.extend(y)              #[1, 1, 2, 2, 3, 4, 10, 5, 1, 2, 3]
print(x)

#Create a copy of x

copy_of_x = x.copy()
print(copy_of_x)       # [1, 1, 2, 2, 3, 4, 10, 5, 1, 2, 3]

#Can a list contain elements of different data-types?

""" Yes, a list can have elements of different data-types.
    Float, int, string etc
    """

#Which data structure does Python use to implement a list?

"""
 Array
 """


