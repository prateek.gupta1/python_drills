#Print all the two-digit prime numbers.

for num in range(10,100):
    if num > 1:
        for i in range(2,num):
            if (num % i) == 0:
                break
        else:
            print(num)


print()

#Write a function that accepts an integer n, n > 0, and returns a list of all n-digit prime numbers

def n_digit_primes(n):
    primes = []
    # your logic here
    for num in range(10**(n-1),10**n):
        if num>1:
            for i in range(2,num):
                if (num % i) == 0:
                    break
            else:
                print(num)
    return primes

n_digit_primes(2)

print()

#Did you write a helper function? If not, write a helper function, is_prime that returns whether a number is prime or not,
#and use it in your n_digit_primes function

